#include <stdio.h> 
#include "doc.h" // Se importa la biblioteca creada previamente con la estructura para trabajar

void consultar_carrera(carrera Universidad[]){ // Se crea esta funcion para consultar ponderados de las carreras a buscar ingresando el codigo de la carrera 
	
	int i; // Se define una variable i para usarla como valor entero
	int digito; // Se crea un arreglo 
	printf("Digite el codigo de la carrera que desea consultar\n"); //Se le pide al usuario ingresar el codigo de la carrera
	scanf("%d",&digito); // Se almacena en la variable digito el valor del codigo de la carrera
	for(i=0;i<53;i++){//Se hace un for para recorrer las carreras
		if (atoi(Universidad[i].codigo)==digito){//Si el codigo es igual al digito, se hace lo de adentro
			printf("EL codigo ingresado es: %s\n",Universidad[i].codigo);//Se imprime el codigo que se ingresa
			printf("La carrera correspondiente es: %s\n",Universidad[i].nombre);//Se imprime la carrera correpsoneidnte al codigo
			printf("Las ponderacion para la postulacion son las siguientes: Nem: %s Rank: %s Leng: %s Mate: %s Hist: %s ciencia: %s\n",Universidad[i].pond_e.nem,Universidad[i].pond_e.rank,Universidad[i].pond_e.leng,Universidad[i].pond_e.mat,Universidad[i].pond_e.hist,Universidad[i].pond_e.cs);
			printf("El ultimo puntaje ingresado fue de: %s\n",Universidad[i].punt_e.min );//Se imprimen los ultimos puntajes
			printf("Los cupos por psu son: %s , Los cupos por BEA son: %s\n",Universidad[i].cu_o.psu,Universidad[i].cu_o.bea);//Se imrpimen las vacantes por BEA
		}
	}
	
}
//Se crea una funcion vacia para la simulacion de carreras respectiva a la universidad
void simulacion(carrera Universidad[]){
	//Se definen las variables necesarias para el funcionamiento correcto de la funcion
	int NEM02,RANK02,LEN02,MATE02,HIST02,CS02,CODI,PONDEFi,DIFE,i,leng_mat;
	//Se imprimen y se piden los datos necesarios para la simulacion
	printf("Ingrese sus puntajes simulados\n");
	printf("Ingrese NEM\n");
	scanf("%d",&NEM02);	
	printf("Ingrese RANKING\n");
	scanf("%d",&RANK02);
	printf("Ingrese puntaje de lenguaje a simular\n");
	scanf("%d",&LEN02);
	printf("Ingrese puntaje de matematicas a simular\n");
	scanf("%d",&MATE02);
	printf("Ingrese puntaje de historia a simular\n");
	scanf("%d",&HIST02);
	printf("Ingrese puntaje de ciencias a simular\n");
	scanf("%d",&CS02);
	printf("Ingrese codigo de la carrera que desea averiguar\n");
	scanf("%d",&CODI);
	//Se crea varias estructuras para el debido funcionamiento de la simulacion.
	for(i=0;i<53;i++){
		if (atoi(Universidad[i].codigo)==CODI){
			if ((atoi(Universidad[i].pond_e.hist)==0)){
				PONDEFi=((atoi(Universidad[i].pond_e.nem)*NEM02)/100)+((atoi(Universidad[i].pond_e.rank)*RANK02)/100)+((atoi(Universidad[i].pond_e.leng)*LEN02)/100)+((atoi(Universidad[i].pond_e.mat)*MATE02)/100)+((atoi(Universidad[i].pond_e.cs)*CS02)/100);
				DIFE=PONDEFi-atoi(Universidad[i].punt_e.min);
				if(atoi(Universidad[i].punt_e.pond)>DIFE)printf("La ponderacion no es la suficiente para postular\n");
				printf("La ponderacion de nem es: %s\n",Universidad[i].pond_e.nem);
				printf("La ponderacion de ranking es: %s\n",Universidad[i].pond_e.rank);
				printf("La ponderacion de lenguaje es: %s\n",Universidad[i].pond_e.leng);
				printf("La ponderacion de matematicas es: %s\n",Universidad[i].pond_e.mat);
				printf("La ponderacion de historia es: %s\n",Universidad[i].pond_e.hist);
				printf("La ponderacion de ciencias es: %s\n",Universidad[i].pond_e.cs);
				printf("La ponderacion de la carrera es de: %s\n",Universidad[i].punt_e.min);
				printf("Su ponderacion es de %d\n",PONDEFi);
				printf("El numero de vacantes via PSU es de:%s\n ",Universidad[i].cu_o.psu);
				printf("El numero de vacantes via BEA es de:%s\n ",Universidad[i].cu_o.bea);
			}
			if(atoi(Universidad[i].pond_e.cs)==0){
				PONDEFi=((atoi(Universidad[i].pond_e.nem)*NEM02)/100)+((atoi(Universidad[i].pond_e.rank)*RANK02)/100)+((atoi(Universidad[i].pond_e.leng)*LEN02)/100)+((atoi(Universidad[i].pond_e.mat)*MATE02)/100)+((atoi(Universidad[i].pond_e.hist)*HIST02)/100);
				DIFE=PONDEFi-atoi(Universidad[i].punt_e.min);
				printf("La ponderacion de nem es: %s\n",Universidad[i].pond_e.nem);
				printf("La ponderacion de ranking es: %s\n",Universidad[i].pond_e.rank);
				printf("La ponderacion de lenguaje es: %s\n",Universidad[i].pond_e.leng);
				printf("La ponderacion de matematicas es: %s\n",Universidad[i].pond_e.mat);
				printf("La ponderacion de historia es: %s\n",Universidad[i].pond_e.hist);
				printf("La ponderacion de ciencias es: %s\n",Universidad[i].pond_e.cs);
				printf("La ponderacion de la carrera es de: %s\n",Universidad[i].punt_e.min);
				printf("Su ponderacion es de: %d\n",PONDEFi);
				printf("El numero de vacantes via PSU es de: %s\n ",Universidad[i].cu_o.psu);
				printf("El numero de vacantes via BEA es de: %s\n ",Universidad[i].cu_o.bea);
			}
			else{
				
				if(CS02<HIST02)PONDEFi=((atoi(Universidad[i].pond_e.nem)*NEM02)/100)+((atoi(Universidad[i].pond_e.rank)*RANK02)/100)+((atoi(Universidad[i].pond_e.leng)*LEN02)/100)+((atoi(Universidad[i].pond_e.mat)*MATE02)/100)+((atoi(Universidad[i].pond_e.hist)*HIST02)/100);
				
				else{PONDEFi=((atoi(Universidad[i].pond_e.nem)*NEM02)/100)+((atoi(Universidad[i].pond_e.rank)*RANK02)/100)+((atoi(Universidad[i].pond_e.leng)*LEN02)/100)+((atoi(Universidad[i].pond_e.mat)*MATE02)/100)+((atoi(Universidad[i].pond_e.cs)*CS02)/100);}
				DIFE=PONDEFi-atoi(Universidad[i].punt_e.min);
				if(atoi(Universidad[i].punt_e.pond)>DIFE)printf("La ponderacion no es la suficiente para postular\n");
				printf("La diferencia con el ultimo postulante es: %d\n",DIFE);
				printf("La ponderacion de nem es: %s\n",Universidad[i].pond_e.nem);
				printf("La ponderacion de ranking es: %s\n",Universidad[i].pond_e.rank);
				printf("La ponderacion de lenguaje es: %s\n",Universidad[i].pond_e.leng);
				printf("La ponderacion de matematicas es: %s\n",Universidad[i].pond_e.mat);
				printf("La ponderacion de historia es: %s\n",Universidad[i].pond_e.hist);
				printf("La ponderacion de ciencias es: %s\n",Universidad[i].pond_e.cs);
				printf("La poNderacion de la carrera es de: %s\n",Universidad[i].punt_e.min);
				printf("Su ponderacion es de: %d\n",PONDEFi);
				printf("El numero de vacantes via PSU es de: %s\n ",Universidad[i].cu_o.psu);
				printf("El numero de vacantes via BEA es de: %s\n ",Universidad[i].cu_o.bea);
			}
		}
	}
}
//Se crea la funcion del menu, para poder ejecutar todas las funciones
void menu(carrera Universidad[]){
	int opcion;
    do{
		printf( "\n  1. Consultar ponderacion de notas");
		printf( "\n  2. Simular Postulacion Carrera");
		printf( "\n  3. Mostrar Ponderaciones  Facultad");
		printf( "\n  4. Salir");
		printf( "\n\n   Introduzca opción (1-4): ");
		scanf( "%d", &opcion );
        switch(opcion){
			case 1: consultar_carrera(Universidad); 
					break;

			case 2: simulacion(Universidad);
					break;

			case 3: 
					printf("Test\n");
					break;
					}
    } while(opcion!= 4 );
}

int main(){
	carrera Universidad[53];
	CargarDoc("carreras.txt",Universidad);
	menu(Universidad);
	
return 0;
} 