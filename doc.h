#ifndef doc_h
#define doc_h

typedef struct {
	char nem[4], rank[4], leng[4], mat[4], hist[4], cs[4];
} ponderaciones; 

typedef struct {
	char pond[7], psu[7], max[7],min[7];	
} puntaje;

typedef struct {
	char psu[12], bea[22];
} cupos;

typedef struct{
	char facultad[150];
	char nombre[150];
	char codigo[60];
	ponderaciones pond_e;
	puntaje punt_e;
	cupos cu_o;
} carrera;


void cargarCarreras(char path[], carrera Universidad[]);
	

#endif

