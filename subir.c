#include "doc.h"
#include <stdio.h>
#include <stdlib.h>

void CargarDoc(char path[], carrera Universidad[]){
	FILE *arch;
	int i=0;
	if((arch=fopen(path,"r"))==NULL){
		printf("No se ha encontrado el archivo");
		exit(0);
	}
	else{
		printf("Las Carreras cargadas son: \n");
		char cod[350];
		while(feof(arch)==0){
			fscanf(arch, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n", Universidad[i].facultad,Universidad[i].nombre,Universidad[i].codigo,Universidad[i].pond_e.nem,Universidad[i].pond_e.rank,Universidad[i].pond_e.leng,Universidad[i].pond_e.mat,Universidad[i].pond_e.hist,Universidad[i].pond_e.cs,Universidad[i].punt_e.pond,Universidad[i].punt_e.psu,Universidad[i].punt_e.max,Universidad[i].punt_e.min,Universidad[i].cu_o.psu,Universidad[i].cu_o.bea);
			i++;
		}
		fclose(arch);
	}
}